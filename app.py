import sys
import numpy as np
from PySide2.QtGui import QFont, QDoubleValidator
from PySide2.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton, QGridLayout, QWidget
from PySide2.QtCore import QTimer, Slot, Qt, QThread

from motor import Motor, MotorState
from motors import MotorParams, MAD_5010

from control_widget import Control
from dial import QDial
from position_widget import PositionWidget
from histogram_widget import HistogramWidget


RATE = 0.0001

class ActuatorInterface(QMainWindow):
    def __init__(self):
        super().__init__()
        self.dial = QDial()
        self.dial_thread = QThread()
        self.dial_thread.started.connect(self.dial.run)
        self.dial.moveToThread(self.dial_thread)
        self.dial.speed.connect(self.dial_update)
        self.dial_thread.start()
        self.motor = Motor(MAD_5010, self)

        self.label = QLabel()
        self.label.setFont(QFont('Robotto', 20))
        self.label.setMinimumHeight(200)
        self.resetButton = QPushButton("Reset")
        self.calibrateButton = QPushButton("Calibrate")
        self.saveButton = QPushButton("Save config")
        self.offsetButton = QPushButton("Set offset")
        self.controller = Control(self.motor)
        self.position_widget = PositionWidget()
        self.histogram_widget = HistogramWidget()

        layout = QGridLayout()
        layout.addWidget(self.position_widget, 0, 0, 2, 1)
        layout.addWidget(self.label,0, 1, 1, -1)
        layout.addWidget(self.resetButton, 1, 1)
        layout.addWidget(self.calibrateButton, 1, 2)
        layout.addWidget(self.saveButton, 1, 3)
        layout.addWidget(self.offsetButton, 1, 4)
        layout.addWidget(self.controller, 2, 0, 1, -1)
        layout.addWidget(self.histogram_widget, 3, 0, 1, -1)

        self.resetButton.clicked.connect(self.motor.reset)
        self.calibrateButton.clicked.connect(self.motor.calibrate)
        self.saveButton.clicked.connect(self.motor.store_config)
        self.controller.reverseCheck.stateChanged.connect(self.reverse)
        self.offsetButton.clicked.connect(self.motor.capture_offset)

        w = QWidget()
        w.setLayout(layout)
        self.setCentralWidget(w)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.motor.update)
        self.timer.timeout.connect(self.motor_updated)
        self.timer.start(20)

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Escape:
            self.dial.is_running = False
            self.dial_thread.exit()
            self.motor.go_to_idle()
            self.motor.reset()
            self.close()
        if key == Qt.Key_F:
            self.showFullScreen()

    @Slot(int)
    def dial_update(self, speed):
        speed = np.sign(speed) * float(speed*speed) * RATE
        self.controller.increment(speed)

    @Slot(bool)
    def reverse(self, isChecked):
        self.motor.sign = -1.0 if isChecked else 1.0

    @Slot()
    def fake_slot(self):
        pass

    @Slot()
    def motor_updated(self):
        state = self.motor.info
        msg = f"""--- Angle ---
Target: {state.target.position:+0.2f}°
Angle : {state.actual.position:+0.2f}° = {state.ticks:+d} = {state.ticks:b}b
Error : {state.target.position - state.actual.position:+0.2f}°
--- Torque ---
Target: {state.target.torque:+0.2f} Nm
actual: {state.actual.torque:+0.2f} Nm
iq: {state.current:+0.3} A
"""
        self.label.setText(msg)
        self.position_widget.update_angles(state.actual.position, state.target.position)
        self.histogram_widget.update_angle(state.ticks)

if __name__ == '__main__':
    # Create the Qt Application
    app = QApplication(sys.argv)
    w = ActuatorInterface()
    w.show()
    sys.exit(app.exec_())
