from evdev import InputDevice, ecodes, list_devices
import time

from PySide2.QtCore import QObject, Slot, Signal

DIAL_NAME = 'Surface Dial System Multi Axis'

class QDial(QObject):
    speed = Signal(int)
    def __init__(self):
        super().__init__()
        self.device = self.find_dial()
        self.is_running = False

    def find_dial(self) -> str:
        devices = [InputDevice(path) for path in list_devices()]
        for device in devices:
            if device.name == DIAL_NAME:
                return device
        return None

    @Slot()
    def run(self):
        if self.device is None:
            return
        self.is_running = True
        print("Dial start polling")
        for event in self.device.read_loop():
            if event.type == ecodes.EV_REL:
                self.speed.emit(int(event.value))
            if not self.is_running:
                print("Dial exiting nicely")
                break
