from enum import Enum
from dataclasses import dataclass, astuple
from math import pi
import typing as tp

import serial
import time
import numpy as np
from unittest.mock import Mock

from PySide2.QtCore import QObject, Slot, Signal

from motors import MotorParams

class ControlMode(Enum):
    idle = 0
    position = 1
    speed = 2
    torque = 3

@dataclass
class Kinematics:
    position: float = 0.0
    speed : float = 0.0
    torque : float = 0.0

@dataclass
class MotorState:
    ticks : int = 0
    current : float = 0.0
    actual : Kinematics = Kinematics()
    target : Kinematics = Kinematics()

class Motor(QObject):

    #info = Signal()

    def __init__(self, params : MotorParams, parent = None):
        super().__init__(parent)
        self.params = params
        self.kt = 30.0 / (pi * params.kv)
        self.mode = ControlMode.idle
        self.target = 0.0
        self.ticks = 0
        self.info = MotorState()
        self.offset = 0.0
        self.connect()

    def limit(self, mode : tp.Optional[ControlMode] = None):
        if mode is None:
            mode = self.mode
        return astuple(self.params.limits)[mode.value - 1]

    def degrees(self, ticks : int) -> float:
        return self.params.sign * (ticks* 360.0) / self.params.resolution

    def to_ticks(self, degrees : float) -> int:
        return int(self.params.sign * degrees / 360.0 * self.params.resolution)

    def mock_serial(self):
        self.serial = Mock()

    def connect(self):
        try:
            self.serial = serial.Serial('/dev/ttyUSB0', 115200, timeout=0.1)
        except:
            self.mock_serial()

    def reset(self) -> None:
        self.serial.write(b'.R\n')

    def go_to_idle(self) -> None:
        self.serial.write(b'.Z\n')

    def calibrate(self) -> None:
        self.serial.write(b'.Q\n')

    def go_to_closed_loop(self) -> None:
        self.serial.write(b'.A\n')
        self.error()

    @Slot()
    def capture_offset(self):
        self.offset = self.degrees(self.ticks)

    @Slot(ControlMode)
    def set_mode(self, mode : ControlMode):
        self.mode = ControlMode(mode)
        # Reset target when changing mode
        self.target = 0.0

        if self.mode == ControlMode.idle:
            self.go_to_idle()
        else:
            if self.mode == ControlMode.position:
                self.target = self.read_angle()
            self.go_to_closed_loop()

    @Slot(float)
    def set_target(self, target : float) -> None:
        """
        @brief Scale target according to control mode and limits.
        @param[in] target : float - in range [-1.0 : 1.0]
        @details It will be applied on next update.
        """
        self.target = np.clip(target, -1.0, 1.0) * self.limit()

    def go_to_pos(self, position : float) -> None:
        ticks = self.to_ticks(position + self.offset)
        msg = f'.P{ticks}\n'.encode()
        self.serial.write(msg)

    def error(self):
        self.serial.write(b'.e\n')
        try:
            rsp = self.serial.readline().strip('\n')
            print(rsp)
        except:
            pass

    def store_config(self) -> None:
        self.serial.write(b'.S\n')

    def read_angle(self) -> float:
        # When there is no motor attached, just echo the target.
        if isinstance(self.serial, Mock):
            self.ticks = self.to_ticks(self.target)
            return self.target

        # Else try actual read
        try:
            self.serial.write(b'.p\n')
            line = self.serial.readline()
            ticks = 0
            if line:
                ticks = int(line.decode().strip('\n'))
            self.ticks = ticks
        except Exception:
            self.ticks = 0
        finally:
            return self.degrees(self.ticks) - self.offset

    def read_current(self) -> float:
        if isinstance(self.serial, Mock):
            return 0.0

        # Else try actual read
        try:
            self.serial.write(b'.i\n')
            line = self.serial.readline()
            ticks = 0
            if line:
                iq = float(line.decode().strip('\n')) / 1000.0
            return iq
        except Exception:
            return 0.0

    def apply_torque(self, torque : float) -> None:
        iq = torque / self.kt
        msg = f'.I{int(iq * 1000.0)}\n'.encode()
        self.serial.write(msg)

    @Slot()
    def update(self):
        """
        @brief Update loop, must be called periodically
        """
        state = MotorState()
        state.actual.position = self.read_angle()
        state.current = self.read_current()
        state.actual.torque = state.current * self.kt
        state.target.position = state.actual.position
        state.target.torque = state.actual.torque
        state.ticks = self.ticks

        if self.mode == ControlMode.position:
            self.go_to_pos(self.target)
            state.target.position = self.target
        elif self.mode == ControlMode.speed:
            state.target.position = state.actual.position
            state.target.speed = self.target
        elif self.mode == ControlMode.torque:
            self.apply_torque(self.target)
            state.target.torque = self.target

        self.info = state
