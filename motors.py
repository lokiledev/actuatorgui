from dataclasses import dataclass


@dataclass
class Limits:
    position : float
    speed : float
    torque : float

@dataclass
class MotorParams:
    limits : Limits
    kv : float
    resolution : float
    sign : float = 1.0

MAD_5010 = MotorParams(Limits(360.0, 2000.0, 1.2), 240.0, 8192.0, 1.0)