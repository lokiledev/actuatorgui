import numpy as np

from PySide2.QtWidgets import (
    QCheckBox,
    QSlider,
    QWidget,
    QComboBox,
    QHBoxLayout)

from PySide2.QtCore import Slot, Qt, Signal

from motor import Motor, ControlMode

RANGE = 100.0

class Control(QWidget):

    new_target = Signal(float)

    def __init__(self, motor : Motor):
        super().__init__()
        self.target = 0.0
        self.modeCombo = QComboBox()
        self.modeCombo.addItems([e.name for e in list(ControlMode)])
        self.modeCombo.currentIndexChanged.connect(motor.set_mode)
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimumWidth(600)
        self.reverseCheck = QCheckBox("Reverse")
        self.slider.sliderMoved.connect(self.user_command)
        self.slider.setMinimum(-RANGE)
        self.slider.setMaximum(RANGE)

        self.new_target.connect(motor.set_target)

        layout = QHBoxLayout()
        layout.addWidget(self.modeCombo)
        layout.addWidget(self.reverseCheck)
        layout.addWidget(self.slider)
        self.setLayout(layout)

    @Slot(int)
    def user_command(self, input: int):
        self.set_target(float(input) / RANGE)

    def set_target(self, target : float):
        self.target = np.clip(target, -1.0, 1.0)
        self.slider.setValue(self.target * RANGE)
        self.new_target.emit(self.target)

    def increment(self, value : float):
        target = self.target + value
        self.set_target(target)
