import numpy as np
from math import cos, sin, radians

from PySide2.QtWidgets import (
    QCheckBox,
    QSlider,
    QWidget,
    QHBoxLayout)

from PySide2.QtCore import Slot, Qt

import pyqtgraph as pg

class PositionWidget(pg.PlotWidget):
    def __init__(self):
        super().__init__()
        self.pos_curve = self.plot(name="angle", pen=pg.mkPen(color=(240, 95, 64), width=3))
        self.target_curve = self.plot(name="consigne", pen=pg.mkPen(color='w', style=Qt.DashLine))

        self.setXRange(-1.0, +1.0)
        self.setYRange(-1.0, +1.0)
        self.setFixedHeight(500)
        self.setFixedWidth(500)
        self.addLegend((20,20))
        self.showGrid(True, True)
        self.setMouseEnabled(False, False)
        self.setTitle("Position moteur")

    def update_angles(self, position : float, target: float):
        angle = radians(position)
        x = cos(angle)
        y = sin(angle)
        self.pos_curve.setData([0, x],[0, y])

        t = radians(target)
        x = cos(t)
        y = sin(t)
        self.target_curve.setData([0, x],[0, y])

