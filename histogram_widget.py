import numpy as np

from PySide2.QtWidgets import QWidget
from PySide2.QtCore import Slot
import pyqtgraph as pg

HISTORY_DEPTH = 50

class HistogramWidget(pg.PlotWidget):
    def __init__(self, span : int = 8192):
        super().__init__()
        self.angles = [0]
        self.span = span

        ## Using stepMode=True causes the plot to draw two lines for each sample.
        ## notice that len(x) == len(y)+1
        y,x = np.histogram(self.angles, bins=np.linspace(0, self.span, self.span))
        self.curve = self.plot(x, y, stepMode=True, fillLevel=0, fillOutline=True, brush=(240, 95, 64,150))
        self.setYRange(0, HISTORY_DEPTH)
        self.setMinimumHeight(300)
        self.setMinimumWidth(300)
        self.setMouseEnabled(False, False)
        self.setTitle("Histogramme encodeur")
        self.showGrid(x=False, y=True)
        self.previous_center = 0

    @Slot(int)
    def update_angle(self, ticks : int):
        pos = ticks % self.span
        self.angles.append(pos)
        if len(self.angles) > HISTORY_DEPTH:
            self.angles.pop(0)
        # compute standard histogram
        y,x = np.histogram(self.angles, bins=np.linspace(0, self.span, self.span))
        self.curve.setData(x, y)

        width = 20
        # dummy truncation
        center = self.previous_center
        if abs(pos - self.previous_center) >= width:
            center = pos
            self.previous_center = center
        self.setXRange(center - width, center + width)
